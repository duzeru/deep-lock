function removeLineBreak(str) {
  var separator = ";";
  return str.replace(/(\r\n|\n|\r)/gm, " ");
}

/* ------------------------------------------------------- */

function unfreeze(user) {
  $("#loader").css("display", "flex");
  shell.exec(`freeze stop ${user}`, (status, output, error) => {
    $("#status").val(status);
    $("#output").val(output);
    $("#error").val(error);

    $("#loader").css("display", "none");

    if (status != 0){
        alert(`Erro ao interromper congelamento do usuário: ${user}`);
        return;
    }
    getUsers();
    alert("Usuário descongelado!. Reinicie o computador para finalizar o processo...");
  });
}

function freeze(user) {
  $("#loader").css("display", "flex");
  shell.exec(`freeze start ${user}`, (status, output, error) => {
    $("#status").val(status);
    $("#output").val(output);
    $("#error").val(error);
    
    $("#loader").css("display", "none");

    if (status != 0){
      alert(`Erro ao congelar usuário: ${user}`);
      return;
    } 
    getUsers();
    alert("Usuário congelado!. Reinicie o computador para finalizar o processo...");
  });
}

function freeze_all() {
  $("#loader").css("display", "flex");
  shell.exec(`freeze start all`, (status, output, error) => {
    $("#status").val(status);
    $("#output").val(output);
    $("#error").val(error);

    $("#loader").css("display", "none");

    if (status != 0){
      alert(`Erro ao congelar todos os usuários!`);
      return;
    } 
    getUsers();
    alert("Usuários congelados com sucesso!. Reinicie o computador para finalizar o processo...");
  });
}

function unfreeze_all() {
  $("#loader").css("display", "flex");
  shell.exec(`freeze stop all`, (status, output, error) => {
    $("#status").val(status);
    $("#output").val(output);
    $("#error").val(error);

    $("#loader").css("display", "none");

    if (status != 0){
        alert(`Erro ao interromper congelamento de todos usuários!`);
        return;
    }
    getUsers();
    alert("Usuários descongelados com sucesso!. Reinicie o computador para finalizar o processo...");
  });
}

function status(user) {
  shell.exec(`freeze status ${user}`, (status, output, error) => {
    $("#status").val(status);
    $("#output").val(output);
    $("#error").val(error);

    if (status != 0) {
      alert(`Erro na verificação do status do usuário: ${user}`);
      return;
    }
    return true;
  });
}

/* ------------------------------------------------------- */

function getUsers() {
  shell.exec(`ls /home`, (status, output, error) => {
    $("#status").val(status);
    $("#output").val(output);
    $("#error").val(error);

    let string = removeLineBreak(output);
    let list = string.split(" ");
    $("#userList").html("");
    for (i = 0; i < list.length; i++) {
      let user = list[i];
      if (userExists(user)) {
        printUser(user);
      }
    }
  });
}

function userExists(user) {
  // Conteudo provisório, encontrar outra forma de confirmar se é um user do sistema de fato
  if (!user) 
    return false;
  var path = `/home/${user}`;
  if (shell.test("-d", path)) {
    return true;
  } else {
    return false;
  }
}

// Deverá dar lugar a função status
function isFrozen(user) {
  if (!user) 
    return false;
  var path = `/etc/.freeze/${user}`;
  if (shell.test("-d", path)) {
    return true;
  } else {
    return false;
  }
}

function printUser(user) {
  var button = "";
  var freezed = isFrozen(user);
  if (freezed) {
    button = `<button class="btn red lighten-3 waves-effect waves-red" onclick="unfreeze('${user}')"style="width: 170px; line-height: 100%; font-size: 73%; text-align:left; color:#263238"><i class="material-icons left">lock_open</i>Desbloquear</button>`;
  } else {
    button = `<button class="btn white waves-effect waves-red" onclick="freeze('${user}')"style="width: 170px; line-height: 100%; font-size: 73%; text-align:left; color:#263238"><i class="material-icons left">lock_outline</i>Bloquear</button>`;
  }
  let line = `<tr><td  style="width: 50px; padding: 0 5px">
  
  <svg style="width: 2rem; height: 2rem;" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
  viewBox="0 0 32 32" style="enable-background:new 0 0 299.638 299.638;" xml:space="preserve">
<g>
	<g>
		<path style="fill: ${ freezed ? "#e57373" : "#ccc"};"  d="M19.75,15.67a6,6,0,1,0-7.51,0A11,11,0,0,0,5,26v1H27V26A11,11,0,0,0,19.75,15.67ZM12,11a4,4,0,1,1,4,4A4,4,0,0,1,12,11ZM7.06,25a9,9,0,0,1,17.89,0Z"/>
	</g>
</g>
</svg></td><td>${user}</td><td style="width: 180px; padding: 0 5px">${button}</td></tr>`;
  $("#userList").append(line);
}

$("#showLog").click(function () {
  if ($("#showLog").prop("checked") == false) {
    $("#log").css("display", "none");
  } else {
    $("#log").css("display", "block");
  }
});

/// ESTE COMANDO VAI REINICIAR O SISTEMA
$('#reboot').click(function(){
  shell.exec('init 6',(a,b,c)=>{
  });
});

$(document).ready(function () {
  getUsers();
});
